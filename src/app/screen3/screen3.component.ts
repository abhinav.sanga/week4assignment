import { Component, OnInit } from '@angular/core';

interface Event {
  clipboardData: any;
}

@Component({
  selector: 'app-screen3',
  templateUrl: './screen3.component.html',
  styleUrls: ['./screen3.component.css']
})

export class Screen3Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  input_text: string = '';
  vowels = 'aeiouAEIOU';
  articles = ['a', 'an', 'the'];
  vowelscount: number = 0;
  articlesCount: number = 0;
  wordscount: number = 0;
  words = [];
  shortWords = [];
  longestWordLen: number = 0;
  shortestWordLen: number = 0;

  // function to remove formatting of the copied text
  inputPaste(event) {
    event.preventDefault();
    let clipboardData = event.clipboardData.getData("text/plain");
    document.execCommand("insertHTML", false, clipboardData);
  }


  //data binding through keyup event
  onKeyUp(e) {
    this.words = this.input_text.split(' ').filter(function (n) {
      return n != '';
    });

    this.wordscount = this.words.length;
    this.shortWords = this.words.filter((n) => {
      return n.toLowerCase() != 'a' && n.toLowerCase() != 'an' && n.toLowerCase() != 'the';
    });

    if (this.words.length) {
      this.longestWordLen = this.words.reduce((a, b) => {
        return a.length > b.length ? a : b;
      }).length;
    } else {
      this.longestWordLen = 0;
    }

    if (!this.shortWords.length) {
      this.shortestWordLen = 0;
    } else {
      this.shortestWordLen = this.shortWords.reduce((a, b) => {
        return a.length < b.length ? a : b;
      }).length;
    }

    this.vowelscount = 0;
    for (let i = 0; i < this.input_text.length; i++) {
      if (this.vowels.indexOf(this.input_text[i]) != -1) {
        this.vowelscount++;
      }
    }

    this.articlesCount = 0;
    for (let i = 0; i < this.words.length; i++) {
      if (!this.articlesCount) {
        this.articlesCount = 0;
      }
      if (this.articles.includes(this.words[i].toLowerCase())) {
        this.articlesCount++;
      }
    }

  }

}
