import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Screen2Component } from '../app/screen2/screen2.component';
import { Screen1Component } from '../app/screen1/screen1.component';
import { Screen3Component } from '../app/screen3/screen3.component';


const routes: Routes = [
  {
    path: 'screen3' , component: Screen3Component,
  },
  {
    path: 'screen2' , component: Screen2Component,
  },
  {
    path: '' , component: Screen1Component,
  },
  {
    path: '**' , component: Screen1Component,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
