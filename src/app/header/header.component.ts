import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  isIn = false;  

  // toggles the state of start/stop button
  toggleState() { 
    let bool = this.isIn;
    this.isIn = bool === false ? true : false;
  }

  ngOnInit() {
  }

}
