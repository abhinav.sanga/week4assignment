import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartModule } from 'angular-highcharts';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Screen2Component } from './screen2/screen2.component';
import {JsonserviceService} from './jsonservice.service';
import { Screen1Component } from './screen1/screen1.component';
import { Screen3Component } from './screen3/screen3.component';
import { HeaderComponent } from './header/header.component'

@NgModule({
  declarations: [
    AppComponent,
    Screen2Component,
    Screen1Component,
    Screen3Component,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ChartModule,
    HttpClientModule,
    RouterModule
  ],
  providers: [JsonserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
