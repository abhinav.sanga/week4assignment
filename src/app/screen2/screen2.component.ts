import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';
import { JsonserviceService } from '../jsonservice.service';

@Component({
  selector: 'app-screen2',
  templateUrl: './screen2.component.html',
  styleUrls: ['./screen2.component.css']
})
export class Screen2Component implements OnInit {

  constructor(private jsonserviceService: JsonserviceService) { }

  stop: boolean = false;
  years = [];
  timer;

  line_chart = new Chart({
    chart: {
      type: 'line'
    },
    title: {
      text: 'Batting average of Virat Kohli'
    },
    subtitle: {
      text: 'Shown in Line Graph'
    },
    credits: {
      enabled: false
    },
    xAxis: {
      categories: this.years,
      title: {
        text: "Years",
      }
    },
    yAxis: {
      title: {
        text: 'Average'
      }
    },
    series: [
      {
        type: 'line',
        name: 'Line',
        data: []
      }
    ]
  });

  area_chart = new Chart({
    chart: {
      type: 'area'
    },
    title: {
      text: 'Batting average of Virat Kohli'
    },
    subtitle: {
      text: 'Shown in Area Graph'
    },
    credits: {
      enabled: false
    },
    xAxis: {
      categories: this.years,
      title: {
        text: "Years",
      }
    },
    yAxis: {
      title: {
        text: 'Average'
      }
    },
    series: [
      {
        type: 'area',
        name: 'Area',
        data: []
      }
    ]
  });

  ngOnInit() {
    this.jsonserviceService.getJSON().subscribe(res => {
      var result = Object.assign(res);
      result.forEach(obj => {
        this.years.push(obj.year);
        this.line_chart.addPoint(obj.average);
        this.area_chart.addPoint(obj.average);
      });
      //calling the setInterval function
      this.interval();
    });
  }

  //Function to add random values in the charts
  addRandom() {
    this.years.push(this.years[this.years.length - 1] + 1);
    this.line_chart.addPoint(this.RNG());
    this.area_chart.addPoint(this.RNG());
  }

  //Function to stop the setInterval function
  stopInterval() {
    this.stop = true;
    this.interval();
  }

  //Function to start the setInterval function
  startInterval() {
    this.stop = false;
    this.interval();
  }

  interval() {
    if (this.stop) {
      clearInterval(this.timer);
    } else {
      this.timer = setInterval(() => {
        this.addRandom();
    }, 1000);
    }
  }


  //Random Number Generator
  RNG() {
    return parseFloat((Math.random() * 150).toFixed(2));
  }


}
