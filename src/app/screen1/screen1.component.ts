import { Component, OnInit } from '@angular/core';
import { Chart } from 'angular-highcharts';

@Component({
  selector: 'app-screen1',
  templateUrl: './screen1.component.html',
  styleUrls: ['./screen1.component.css']
})
export class Screen1Component implements OnInit {

  constructor() { }

  data = [{ name: "Tata", y: 1000 }, { name: "Toyota", y: 1500 }, { name: "Mercedes", y: 2000 }];  
  inputName: string = '';
  inputValue: string = '';
  valueError: boolean = false;
  categories = ['Tata', 'Toyota', 'Mercedes'];
  
  
  pie_chart = new Chart({

    chart: {
      type: 'pie'
    },
    title: {
      text: 'Piechart'
    },
    credits: {
      enabled: false
    },
    
    series: [
      {
        type: 'pie',
        name: 'Pie',
        data: this.data,
      }
    ]
  });

  column_chart = new Chart({
    chart: {
      type: 'column'
    },
    title: {
      text: 'Columnchart'
    },
    credits: {
      enabled: false
    },
    xAxis: {
      categories: this.categories,
    },
    yAxis: {
      title:{
        text: 'Displacement(cc)'
      }
    },
    series: [
      {
        type: 'column',
        name: 'column',
        data: this.data,
      }
    ]
  });

  line_chart = new Chart({
    chart: {
      type: 'line',
    },
    title: {
      text: 'Linechart'
    },
    credits: {
      enabled: false
    },
    xAxis:{
      categories:this.categories,
    },
    yAxis: {
      title:{
        text: 'Displacement(cc)'
      }
    },
    series: [
      {
        type: 'line',
        name: 'Line',
        data: this.data
      }
    ]
  });

  bar_chart = new Chart({
    chart: {
      type: 'bar'
    },
    title: {
      text: 'Barchart'
    },
    credits: {
      enabled: true
    },
    xAxis: {
      categories: this.categories,
    },
    yAxis: {
      title:{
        text: 'Displacement(cc)'
      }
    },
    series: [
      {
        type: 'bar',
        name: 'Bar',
        data: this.data
      }
    ]
  });

  ngOnInit() {

  }

  //function to add a point
  insertPoint(obj){
    obj.addPoint({ name: this.inputName, y: parseInt(this.inputValue) });
  }


  //function to insert an input value in the charts
  insertValue() {
    if (Number.isNaN(parseInt(this.inputValue))) {
      this.valueError = true;
      return ;
    } 
      this.valueError = false;
      this.inputName = this.inputName.charAt(0).toUpperCase() + this.inputName.slice(1);
      this.categories.push(this.inputName);
      this.insertPoint(this.pie_chart);
      this.insertPoint(this.column_chart);
      this.insertPoint(this.line_chart);
      this.insertPoint(this.bar_chart);  
  }

}
